# Emerald DAO

## Summary

Emerald DAO is a simple Bitcoin Cash DAO template which acts as a fixed-term deposit savings vault with optional crowdfunded rewards scheme.
Each depositor interacting with the DAO will be creating a time-locked safebox UTXO and a matching keycard NFT UTXO.
Upon expiry of the time-lock, user will have the option to burn the keycard NFT UTXO to open the safebox and access the deposited BCH + holding reward, or he may keep the keycard NFT indefinitely as a collectible.

## Technical Description

The DAO template consists of 3 contracts labeled "blueprint", "vault", and "safebox".
The "blueprint" contract will be placed on a pure index-0 BCH UTXO which will be consumed to create the genesis transaction.
The genesis transaction will create exactly one NFT with "minting" capability and lock it with the "vault" contract, which is a recursive covenant.
Users will interact with the "vault" contract to create pairs of immutable NFTs:

- Immutable NFT, sent to the "safebox" contract, which will store the user's BCH deposit;
- Immutable "keycard" NFT, sent to the user's funding address, which may later (upon expiry of time-lock) be burned to unlock the matching "safebox" contract.

We illustrate the DAO's operation with the figure below.

<p align="center"><img src="img/fig1.png" width="723"></img></p>

It demonstrates a few contract-building patterns:

- "pre-genesis covenant" - requires correct setup of token genesis and creates the main covenant, it is also used to reveal preimages of child contracts.
- deposits are serial - by respending the main covenant, which keeps track of the number of minted NFTs
- state sharding - the main covenant emits NFT receipts with one-time covenants to hold user deposits, the NFTs are coded to match with a P2PKH "keycard" NFT
- NFT-owned UTXO (safebox contract requires a matching P2PKH NFT to be spent in same TX)
- async/parallel withdrawals - user deposits are each in their own UTXO, so everyone can redeem whenever (after the timelock expires) with no contention with other users

## Source Code

### Version 2.1.0

The 3 contracts are written in CashScript:

- [blueprint](https://gitlab.com/0353F40E/emerald-dao/-/blob/main/contracts/v2-1-0/blueprint.cash),
- [vault](https://gitlab.com/0353F40E/emerald-dao/-/blob/main/contracts/v2-1-0/vault.cash),
- [safebox](https://gitlab.com/0353F40E/emerald-dao/-/blob/main/contracts/v2-1-0/safebox.cash).

[BitAuthIDE](alpha.ide.bitauth.com/) template testing all spending paths of the compiled bytecode is available [here](https://gitlab.com/0353F40E/emerald-dao/-/blob/main/contracts/v2-1-0/emerald_dao_v2.1.0.bitauth-template.json).

## Deployments

### Version 2.1.0 - Bitcoin Cash May 2023 Upgrade Day Celebration Instance

DAO Genesis TX was mined as the 1st TX in the 1st upgraded block [(792773)](https://explorer.bitcoinunlimited.info/block-height/792773),
with an OP_RETURN message: "Happy Bitcoin Cash May 2023 Upgrade Day!".

Main parameters:

- Maximum number of safebox NFTs: 2000
- Initial rewards pool: 2.14556600 (+0.00102277 bonus / NFT)
- Minting possible until: June 15th 2023
- Safeboxes unlock at: May 15th 2024

<p align="center"><img src="img/dao-community-banner.jpg" width="400"></img></p>

<p align="center"><img src="https://ipfs.pat.mn/ipfs/QmcNL1KcVmiDtwJe8WokrnzYeoHirsz1sNxNojncsxyb2p" width="400"></img></p>

- [Reddit announcement post & minting instructions](https://old.reddit.com/r/cashtokens/comments/13g9huj/announcing_emerald_dao_v210_nft_series/)
- Category ID: [180f0db4465c2af5ef9363f46bacde732fa6ffb3bfe65844452078085b2e7c93](https://3xpl.com/bitcoin-cash/address/pp5em6u9xt9cpuf7fyvvv9cl5yqj3v274uvgt59zf5)
- Genesis TXID: [00003c40fa202816c357350eaa2e7ec2b47766209604941789ecf814f98ba4a6](https://3xpl.com/bitcoin-cash/transaction/00003c40fa202816c357350eaa2e7ec2b47766209604941789ecf814f98ba4a6)
- Blueprint contract address: [pp5em6u9xt9cpuf7fyvvv9cl5yqj3v274uvgt59zf5](https://3xpl.com/bitcoin-cash/address/pp5em6u9xt9cpuf7fyvvv9cl5yqj3v274uvgt59zf5)
- Vault contract address:  [pqzwlpeg7m62r9h7w0js2uh2250d8hmkjysafc4su4](https://3xpl.com/bitcoin-cash/address/pqzwlpeg7m62r9h7w0js2uh2250d8hmkjysafc4su4)
- Safebox contract address:  [pr43rx2gwdq6j2dpmrpxldftu7swfn7xvqga6vzmp3](https://3xpl.com/bitcoin-cash/address/pr43rx2gwdq6j2dpmrpxldftu7swfn7xvqga6vzmp3)
- [Web dapp to interact with the instance](https://emerald-dao.vercel.app/)
- [Web dapp source](https://github.com/mainnet-pat/emerald_dao)
- [Other instance particulars](https://gitlab.com/0353F40E/emerald-dao/-/tree/main/instances/v2-1-0/180f0db4465c2af5ef9363f46bacde732fa6ffb3bfe65844452078085b2e7c93)

Community sites:

- [Telegram group](https://t.me/emeralddao)
- [Twitter fanpage](https://twitter.com/EmeraldDaoNFT)

## Tests

### Version 2.1.0 - Chipnet

DAO instance contracts:

- Blueprint contract address: https://chipnet.imaginary.cash/address/bchtest:prqd82cxtdfgss8x0ffe7h6qwyg9c9vjtcjgdjvszy (sha256 of static "tail" part of redeem script bytecode: `c2ac9207655ecca85dcdc3c0fae33233a719afcf40a92a41e02911bce41ede1c`)
- Vault contract address: https://chipnet.imaginary.cash/address/bchtest:pzdh0j90jl7f4xpjmr5x5676czug7lsc4cwtqk736l (sha256 of static "tail" part of redeem script bytecode: `f1e36e373a10be68444155b0bd4ef81f8d1973693534c129cb19f451a9a94fe7`)
- Safebox contract address: https://chipnet.imaginary.cash/address/bchtest:pz4s5whwpzrxrqa065n8jva8pzdv44hx9qepr7u0st (sha256 of static "tail" part of redeem script bytecode: `09b65c077a62fe065580ee949ab77a695ff82806915fc1b3237f0c34e96f7f26`)

DAO configuration (from configured [BitauthIDE template](https://bitauth-20u91jodn-mainnet-pat.vercel.app/import-template/eJztPYty3DaSv8Kb3Yrj7EjmY566rK9kSYl1sTQ-SXaci11TIAlKXM-Qs0OOHrflf79uPEiABDkc2Um8d-tKRRIJNBqNfqMB_qP35yy4oUvSO-jd5PkqO3j2LA7pvh_nZJPf7Afp8hn-QpM8Dkgep8leTperBcnp3q29z_vu_y1Lk16_F9IsWMcrbAXgTpZ0TRahdXw4s-LMIlYWQ0dqvYjzII0T64hkN-ylBGjd3cTBjUWCHFpjhyi-pyGMt15aIV2lWZxbGbmNk-vMuiWbRW7dxfmNlbIBycIK1uldGG2SkIbWmt6RdZhZDEG6_z45IQBaQEnXVpwAWBgJgHEoMEWGzF28WFg-BWCUsLfEyuMl3VukwUeAm5GI-um99ebq3cwiSQivlyQPbrDlR_oQwKDW-Q9X7D2M-maVJha9X8XrByuN2CAFtL61yeiaD3hDbil7yydj5anlb9YJe1QFiy_TFeUvJT4MlSCgWcYei4kCwi-OXlp_sW7SRYgocrL0LaAAtFqSB4BOV7VhYqBhFCcAYPHAlyJIFwsK5PIXdB9WOiFLWlniW3ff2bfhHbJKHtOsd_CPHs4Qf-qcUQJ4g-_7vVuyjgmALvrMAZu2fkWbfm-1jm-BeY4p-wkNX5P8Bposn9nPEJ38YYU9XoY_QfNPfd41i69vgP3mSZTPgcWXcb4EtOsjnlOK3AQUv6YJzBWYNEjXa6AEcPN1QvLNmsIyImmBwrCat3Sd4QLCYp8en_StgCTITWu6TG8BELL5er8yjwZkCtQPw3ANC3tMclKbAMgkvU7XD_M4_IOxVzExo_4JkA9JWseTaYgky0kSIIZJFF9v1mwtrRVZw1gwbKaMC-2rTANw57znfEnu50IsxLsGJmrsU6D_MwGuzyXhlfai7TxJlzGonvktWWxox7HMfbuPibI_Ry2y43hlv-1jMfU6DxZpRncZqtatYSRkxyzO5vSeBhtQx_OItg5hbN8kH7KxHywZd3I-7wK-0qNpAGXCwszAzyVBpbme-wArioOYrFsVWGcYWyjIUE43-WqTz_2HnAZp2H2q1X7bKLqEZ-S6E3zZdBtIgAP0hjmvNv4Wnd_UZdsQrSqyDn67HjNyAMhyvNws53ECnoPZmLSufr3_dintKj5NXbaPEG6yfL6IwSR1hK902A4dcJjDu_QOFX_HAfQ-xjGYnRFMYjA24s2TzAIGWsQBej6F23cDi7GHwhCCRZJOITT1QUeDp5HkfeZq3YBDW5pQ2XlN_76J19A1Av-K9xNcZV29A2_wCh22oleQorm10gR8LBxIMt7pMRsiitdZbnkKBIvLa2ah94hGWwpupsCG0aElwwiArimgQhG3JRIIzSW6A4yAMMpDmrC_gwWJlwwJP90k-QN3iYMbklzjOPhigJ42G_9JHQEYf4a2u5wcILwkIRUk7sux0KFgYKk62oo8AGhE5CZeZnQRIQHeJzk4zfgMfWMkuOKdYGeFMCA8NFQdBLn6VS9Bqo4tqkZr1-ZiugYXk_FfFtAERk6rzkmIHMoe8Z-azjbLXW9i4yDNctNzRtUWDf5Jz7HZv15XFWbuoOsdc5uKF9Zzay3qPgZMZDIYeHVgBtdHtHUaZ9Ngknv2_XhEps5gOBlFnhd6QTgeeTRwXEqciTsl4cQbBCMKbwZDx3P8yYQEPaFSKj6r7qrikvZLHcBCuBiCo7csVEWRvhTBWl1-S9aTKrIw1FLxtLKO2aHqDeXCtLsLBU2o4_iB7Q-GNPQnwTggAZn4Iy8ahLZLAzJypqEfuJ7NaGLwC3pPXpLV6kEP8c8gynRt17M2q-s16oSQPPzbk16LD6DJ3z5XIUwUzcsgFUFBqsaleFG0qGkwDFzvc5qEWcWgIbUbVye_37YimvcBhKbU9wgdOoE9mFJnNA3I1HYiMnKng4HjjH1n7AYu9YewBpE7JOOI-N408IbToT_0pv54Cw3yNUkyzGsAQy6YWq3PrIG1ykmCRivB4GTiBO1O7-BXiM4TQboXxWR_7WWLNO996PeQs2Dd86uy-0tgAYDpjwF5mARMJPC9iJCxCxMcwKRc6gbOGCbuDAYgfiMnmtrAakCcwTSwhw4BgjFfRQI_BaEGstufxIASsxpe_-hxKlXcXlxDpg8vSZ5mYFh6B6BhP_XbISDhuMZauTCjOozBlEkb0C79SBnV5NJ_meknUc6BrogfL-IcwYLGxhwVvFWTGD0m9GiGuhElXYGyBKudFIJcn52ZPr0RsZm6Htk9Q5cPaAjTzTqgs7Z1KvmnAoGrLw5HlzpQXnPNxRMSeIMmOOO_veZWWzz41F1uZLIkBO0gsml8pGZRAj1QD7EUFI0TKJJjLeqjrhvtexsVsuNGoBaiKIgGNPImke0PiRsMR5PRcDByhsTxHd8LXW8yHoWBQ0ch9QZ-ZHvDsedNqxZBiZIBvmcPhrbrgmkNwSZGYAI8fxQFU3fggBWn0yEYRTsEBrbhMQkd3x6N6cCdgCYbjqNoMB0RfzDww-EUoNhDd0zJJByQYDD2wNT69tQmNh0MgmAwGg9BA06jceSPQ-q60cChdBI4Uyei0_EQ_vNHSLsvv6zghyL5d15RtmZiNavrtxOWmPnMWOgBYBhKhcto3aHjzrPDMk-daVnqjtYKx7BE2rPUXyI3PAcvgYQh8qJU7waHWB9lw9O1v6GdsCeODwbPHYa2PRyOp04IzoY9HQwHwN90DBwzCQM_DMbwwAuHwNckCEdB4E8Ho2AcjU12om_ERlGELI_pBzeKHHz6fZF0PltXuuDRDkejr84AVdB0XGYvAMtOC7JyVx_B2BYWQOdg4X1pPHuE-zYUd5uEsy0a10VmizQowSBrUed21fmorwZwg-d6X8VqOJ3cAbmngrtAbC9CEN_kLf2u00rShJrmBKZlNCV28a_DHHHdZTDZ4Mc5ANp1x-OvYoo4pQ-fjMyP9mDOcylGQdjC76p9M4D8F7f_i9v_EG7vsmyKZahNaDpxvJFXFRqu2utCchiy7ICsFlil6eLxhgIe7yY27ldosg2E26ZpvjQRH6-HvkaCfj5DTxyVoaVAI5HCNbkji5YsVGuxA4aSENBFo8HUjhTF0lZiAH2m3jhw6dC3J-OJC6Ei6P4JHfkRkBTzWNHYg7AxgAeDkTf1aBQNXTKEWM8f2KEdORNDButnMRcrWqdLra6F1dFsiUNKStS5BUnPE8Zlvri_c5wivHS3LSgYIcYuccYendCJP3Lc0dDzbKAVcUfeAEJy4jlhZE8idzoZjYgNBBvaZDoZR9PhKIgA94z-fUOTgJ5vlj6Gll1DF9WMGUMYGQq14T92RtEwCELPicJwBKJih-EYQv_AHoSD0AudiU1G48AbhdF47BLfGdEQJua7k8F4PPRCI_47pOi2CoKjZIN2ipT-aMvmMkVgmo8zHQ8e76h8BfOq6Q7DPCdKBk8qr2BBSTJfpYvFBlmwmwOrS30FxGOTEZ_DoruyYiWMFVtfu0-9da7NEh6CZI_tICATSuiUDnwyCPwgcKhDA-CCkTsejYfu1AN1PR7YJJqEkRe6BBz7cTQYT4kht9JvJ-0n43zZDh72mkOsHtxQc1mGyX0oKFaQogaqzQKMneF4PBjY9pfUSp43_qpVUpN70qSV5Hw6rBt31IqN1p2Xsc4IjetaHarVLfz8Be28GWTeZa5DHY-9wf8pNgFl7w4UzV5ZWW37_wvIuRneb8wFj18vssTKFqYglaUbg2cFrnAIKwSrB0o1BJfZjsaj0cB2iT3x6NiLvOGQRONo5E_90cCbTP3Ad4bUd1yy49KFtj3eVcKNZP5sMW9avLa1_WcX-OHnuapfNQM1yf7_U5-OlXshuLLYi21CVDepVyTLsBbq17bN7A960WMdUL_E_dkzVr-SyWj5goaULiWO75PvFZf7-fsEmmPpi3iATuFrTMYAaaHpn7_9vljK59b3znNr9np--frV6RX-cnwxe219P2APD4-Pn3JofHDWxTg0B8aaYnICJlBspV_KMBWbqtU-2LAMYvf1bWqyyGkIAHvS78zU3VlZzlJUmRbEkzu1bSvA2jRRX7Dpl6O8OxwpxHenX5Sg5r39TnQzp-nSoqoupP7muk5KLcu6JX_YsJPXYY-jXJ1uaOoLhmlKrMvlnRgR2b9TplMOLNvew5LchGCRr-3s4UkyEPmyodAH1rcA6Wml_a_QgSP6wdBBjFntBH1EYA-_u3uCPRCYVwLT1qwgEa6VIRMIlKjq0NpatcTgbRnGkvjdxtWJ_3PR1kj4Szl1IMlPnCQ1MrKGwO5gqgiXG9bkCMetA82xYjhOsPDYQk3NSpCtFGuiLWZFDPChRX7DDaFOcjHj4vSVOeEGlK4cTzM0UsiCOle2xJOUSbpezyv6rwZJVrHI9Czoavt-wNX10eEVauYSaFk6-VybU3NOyVAKUZmW_lKdTm0u5TTIYiHKTrMmBCucXnfbtpKibQFqbXX2jCNxDo4sVjdEPf3at9AnAoeIcxSW8W6S8hHW1i_Su_cJrCVOF2DMYeyYLOL_YVy6H21g7ur8mWF9eQgW4Clj4e8sxopnJNmQhSXwk9VKWLiEpwlgxBX8hS7bA2vOj75WT-0JxE-PTxCj9wmydwNeotfz1kaK-zWXuZ9szvmOM1zn7jI5PE9YdngHKHLguQqu6H1x8vbk4vLkxS9XJ5c7QUMv_l5HAOi6t-fTa9AbcfI3PCfJHHJLg7C3J-Db9zSqTKBp46TSzL4f2ZVHTvVBy9aNAWlwqQu8zPNmx00grpLbQ_MFTa7zbitQ7dt52TY5P3rQqUOFQ3bmMCFg3RlLpgZrq1PqU6t4xhPcetNCkt8nIMvfPeuoZJXwuMmV2ho7PzZ475aQfVT6r-qe8QnqivYIn7X6Xma7Lw82_EApd7aEp3chR39RRuOtTpMxVm1ehZqLVJ9km9vzezgnlak1O8WsBKTFdf8D_HSGkk6yNQt8LP6oEveY517UthgcU_OcfwsXuONUZFDY5GMWk1ECUPMkdkkgGJD7kvHrP1XywLAwtVD4eXukrC0Sl7-DIkd0xU8DYhPX7mmrwZua1qFFnQl6dDneplI2uIkXoYirXunZK3192WTKRVb7nfPjg2_RhOtoGA8YlkDAFF1KS6R31IxU2YFRhhmHV8Io692qhwXLnoIgZ_zw4qk8u2imWu2IYwlHsTB6X_VMXdkcT2i-wgOaeuPy4GbZFPodytOaemvtIOdzEdCepzk9sE4jbiWgZY4HAPBE7WpBAhpaaVIYA8vf5BaxVut0BaaDnTRkMKQxZfUzfetOnFHm536zFWWZDX4Ogp3LFbbo6h0bj97n-wIZiQaOjud68ToZhPk-AWE-PX_95ur0_PjkHfsTn1_Nfjo5B__o5MfZxS8o8DZ7dfJfbw5f8T4_MLgni4wCq9HgI552YIYPgyl2vw27LAgYKpZnJ-XdIPsc1qvLk8Lzfas0O3tzecVPCy_x0HMO9khdJuGwlUhbAue3h6_enBTvXfzf69Ojn4onP16cwIQurl4ens8uxEzah-f5dHaVTplob8FApxo4m66tq9Dz09clgsVvCi7w14vZ7NXh-XEzbuBbg_ADowM7LR4sVygKCBvL0GIrkrOzs9Ors5Nzhtbl6X-f1NBjD96c7YhccQNSiQuLcYEzOSus0bvYDb0Xp-cuYFJ0svF_E7m--NMBe1S8_vn06uXpeXeUz2cCbWJFm-Q65ofrMV7kuzfbkD08m705LyMJu_htK_UOk_K-HOZJchLFkRUzRYGYsUclyNkVSp4l_gEMvNPLYg7Q3mZlCR8ImFbcXFWMxSRV6gCzXPI7EVgbH5YpuLGWIHYFBPwDVE-WAgdmGy6ZOWIKvR9KmMWM7hCzzSrEs1r75ZxRd1G85QqdjuygDH4tbhbQsojIxPoWb8_KBD9xVfi01qEaz2zrJe8g-1ZsYWkNGQkocG5W3KT1TVHFCIogUyFVveA0XZihPi1lvdB6AsoZ-UgtQc6KnfApLiS3W0ixjAcVlr1fQqiob-Wxrf0lefHtycXpD7-UrywNGTATnCUkKVUHgLNaLKQG71ZD8od9DQIaMs6_EkTdJSgBHf6id5YwrSxm91CghcNYKrOiNeVXXoR_QzbM74uoXyeG3VWPiPYj3UwYJVc8vHr3anb009Xp2UkHCE3GRtEEswt9GRW5FvRgVJPuQO3tBT8XXxgCh7PHgc5I-3o_Ng_GMUel1lJeOrUnjZyjMY_A6WeqXIQH2gH0EqyWI64ckf5JGtU6FilYsLQQHPWFUyFVjOK31CfE1nz25gompVlgc8PukwFp4O45yygzW4CI5GlqIaLIxbU-K_KAk7XoLV0L3w7kVvqe6EM24F_xYJTXhSdT-wOsHv4YVJ7VILw6ubxETqy9qPKceAyoMGoaWES0cLZxRmlx6Jo-QetQRDrczJR6xEBDMCPiJpnWgKrST9z04yah4Lc6qXX1q_SVwuRyIcqYGaMkRLfKeSSR3G1Ewn9GbE5K7kGFqkYe1jfGHkrYY30rMqi_2h_2WUD31NjlOkWdnir0Kns6H57uNy88n7iBW0WjNoZWaaNrTeXt5ZsXxueDR_Wq62J8utOCHHXURnUqtWgklVq784iOklzMzvJSxxS3Xo5mx-b1mqo6phFZfHd-XPFQmR64o0_QVMUsRAxTfiOr9MTStdYB3Cp2XZV-FkkzlnU5hn7HBTjLcJapgwVVhF-iitbooNZxTzQECWMWQTW4_97c2hGtqSWKEviLHU30VrVSQ-AtXcfRA-cVbeR2u-oY49rPtaoCGfAs962BvAvNQIFG7Sr1gLRqW2S6hsCZEtazmAbcTpaSxZRM3TFJrVKdSgcGuaLFjnegVxd3ZQeaMqe9iGF-m1mZ9YM-ocY2O3ldKCFX78CJzEAkQZoLNjH7XNh8gX6msGIyPiEszSXuqWvyCNoZjcna2ey4mGY9MhAtTa6UDHcZdREXZo3RIeRI9UFzP8lq66NMi10egjf73aQYCtwArOUGAnN4hJ5mpGpOpSs4DMzJ8smC-QyCoOLW4MXDflUZin5HKq1KNhIaYrsbxLQGmOHfxfzVUGVBR0YgwJTXgsmdAUN_WARNFeI9kmt6m9Z4RLS_EqkToDy7AB11GJCUXUjGrBmQGwEUybdKf7aSdL1O15nZZ9hCyFbr7FRldEcrXacyF0SYIEw5ScEVXz8Im6zc-8hzfErCUenNYnxu9c32vEMcV0Tv21Rtc8PdhHW71yBanqH1cqVa6gsF_JfyEvtS83xO8NDRxOG_1qhG7JdYxX4JUyCcW_FKUXAQ0022MLiIbJet2GnARX9bXtmf4W2kJdAGxlaWtWvIgH8MW6Na0e0LuPZ8n4b4YDxxg4Us1hD2PfD8AxhPWf1jfaPtAbSAQiJxcCBBWQ796wIiOlQH0xPnraM0Ily9EtHg9zKPHBzlJSVJJoS04pPvEDoXlbhmPU0WwYZ9FmJBo_wqRe_LuJTaendI4zVGfNqg5JbEC7w49oILdUfm81RkhB4eVJ5tGxksf57TkA_cIhnArczLOHxXS_Acn77tyMW8vsSy7QNzvlA0E853kip7azqizMaRj1U3S_QvDvazQZ7IDW-TQtfmuLP0Py4_8HWpAzXQULbDpFtV6E1-b5yzlX6ttq5KyXbBcZoUqgwuXWj8OMfsM9WTgCL52TlQv2jS0PgkAUc4KON4dtkj263iSVteeZtkcZbTJNiaCGnmU01HPSqzJwRQuST8G_6pluVyk7N7vas7zc14dvStDY1Ne9NY3mME8uhJVvie71tnlp-KGxDh_Z_MZjEJRajEt8BEwUSsfSunY-atg7h0b-5WmAT_nFQkpqgq_VJklPyPd9UjHX2Ceb-0CF8I_6jBZ6X3agHE9im0iq17oG6adpzvzzcgFbcQJgmjckcwO7VY4E380u6wPdZyW7oCweehX41XgFzXvDqluH4TC8CCeEWazFZHO1U1b23WCSx8J64yr5Cj6p7qNtBvZQ0_W125VQFrU1c76bYvrZakRjLzQ20eX1SvPHoukr-R67fh3a4A2h2ux7oBccR3AHnRiZaKk5tNlu097RtB3FGxjyxciFucc0zZ3iHzLxrTWFgOY21WZoqwOlb5y7bMiP6k_Kv8TRuvBt_8Cx95xrbbZf2pPJzWqQJVNq4cf3p0yeYFxW8ymGs2a19t2K16UtYf8uog-e0EQ_GhplvZu0r6o6F9RVXMtHo2xdMpU7czpRSrBWRzjdysVh_3GJBFPYpUGCzCvJi9eiVddPwpy9m6jSCL0Wa1fHW1v2BpTG2KAioembDPHKLnxT45s2H3W8sv4xBRvJWnRee76p1v8doquGy_EJvjN4zxj16eHP0ky2i4PtF9zwLuSWIoisIdAfnpO7GBhae_ilGqpVBVKqgarBipaRdQLU1r3u7rq35GvUe55Vf9vmSBobrZx7RE8aYV7YoFKAJTNo6wZ2IoiNPl66J7oZ_Z1zJLugIBZAVf2OdZXLm7JBaBV_9lBSRok5UfTyo9A6yWs7IU1XiYJk9yK6H8Ix68tMdUVQvQ0Frs66vXsKU2a9qhNBqsJrrxo2QWP6tm_fWvhbutvaiQzUwRy9nL0z0HVmC1YifOzZOouAL1aWhKwq0Fae3Tq1eFcTPL85KYli4Pu4hXCifWstKzhlofhSCVdcVgG4cRsXeKxYD9kqq8pO8J_-4W5ac_BCjxJa04F3Wo4ktYayukQYwumrr9BSb3lrkSbMCyrgrgXJ7--PLw8uV8dn6CX6oCzsONUC62V-9EbSGfOqwWb0Hv4wzQBHZFxlAV3IZXMVU-rGodpXHyn_hxnKt3hZ6r8hjbcBdeTiGfwGJ2qXoRJ56GZY1ZZEJAhQLZmPzgphPuNgUxLiYWtjJa82hG57BGx3nWtMlWxReXDSsCsP4K97lwK01Elxw9lQYBvkWlb8JC9zVNAaYhnpSZab2Iuc9e8CMPiCB-BYjyT53Jb0Dg2Yd91SczuFgNxzhr3haqvhDalP5WQ0_F9eL2jY2GjOeMbH6YynQ-3iob4QmqKhWkjQQe1r4PWVzusdU9rHfRnUT1Y7_lh5T4Z38tsQw_lt9k-yFdL8Uxxj1x8LE41bjH8s1Mrr4dQKieod0Bmd1_WvSYyfoOrcuLo7MLC0-4Q0QAIgR2J17FVDk0uscyf4hieRhGPHcPmJd0cvXm4nx_f1959avtHVgnSqllCfiDmFj9hJDpk1uPOfOzg0eMJY7sMzDFSbjX7E8Fm8r3YqQbXWT5aycX-IEFvZCjVTEYlYL01fHj22vxCW7mAohvcJUHDUA_6WLKnWK5mSF813rp6tmbV0XKYov3r6Qvqh4rr6pgn1x0rAQ_Zs6mr9V47GbPpO655ZpQ--h5cUQjifPy2Ln1rVisp_XjTD8LV1miyJzBPv8YObAm87NX6_Q2DkvrR5N0c33DLTSSnLmMH4UjVwqqONdh8B3bJ8j1fVXjSz57on4HM41K11fJ_PxF0NcufnOK39y6EZidnl9dvUNV12YgtIzpzJiwrDYxpDREE-YqiYP8orGWsZfcqe3uMXV7fHh1CCrXaBxPEyDBGnxU7uBUSSjWMTSdgZXOuOorW5U7yPAcoMyEiErjZ99ZqNU9V9QT8KcQyRIiYtmxOgOkMcxZEgHaTcb6BQffPWM_EcLUGZghgEFqgdC8NDUuK0nDbkBBTSFJ1HBMWfSSkcsduBrAkBQc71D5gqvxjHEzuWaVM2KCShpBTG2Y9RYANCJpJbDMIy_TRAIFq4KDAKPnIRp2XARKlT5itQx9Wh06ZTtYt7WKCgA9XxhSruX5novcn7S3ya3TgBnei0KeFwiW0bmeV2veGW4L26WiLiu-CpNbPa-nVyxsG1FTCqaRZ1oqsBIRVrc1LfFlYfEVYb9UtcoKKJuciqtTvOYV3rf81HZdCPXzrk0191UdqG1OGGhiajMyApi17zBoRDpSQn7jBnp9els3LFGbt5kdo4TMilSU26S8SlyX7CA98hS70EO1_s0Y18N7uxiubWO9GDlJrR-MWtd42rRFTnbnTKMOqTncrQxq3szagmkxcebHmWDtEtY-Yur9oiemHu6Kb4BjMgIj4ZysRc1bQYpt_khH7VhLqEiyV2IZVXFnYlMlS5eUnxLC2qr_6KILzKK8gy6oKqCqStCUTdMpyIoxKDz6stAbTxnL2ePXxrEkiXkB9D5fEw2jhlrKmcinN5VpNLCd6Oq1sl4r-9UsD-dH1cHH5WIsqB4M1A_Srit5PK8oNu04ea-bHZuZtqXKNMq23SyeYtGuHNwtsVL2-F0SKpXbhjpgWemhJlO-Y_cFzjQP_PWby5fsEru558pL-ZRLdVRH9HuJa3FLGf_B7iBDeS-BC-e8BA7GsBE4J1cT8Mp-40500Po8lhLyPqLfiBZV8O3U4MnBhpuBOlCloedjaFO_veg3oFDzIDvRqbyJSbmt0_RWIURxkaXhm-o19AwfuK8xvLzxSOtZ-wr280LPldMRP4o3xe1_6iTr39OuT7XepnJxHEv_8Y1MKj9nLNvOS7-ER7g5Xa6wdpinvMsIByzF0vr-iej25DnP7YnI7fsK1OfVhWpYIRXTNrLrWtN8b345wraWff0y_i63YbG767PNapWuGb_92gMTCp7VyRH7fi6__xS_UvW_p-sagg==)):

```
    "dao_config_data": {
      "data": {
        "bytecode": {
          "dao_config_dust_limit": "800",
          "dao_config_fee_allowance": "1600",
          "dao_config_safebox_nominal_value": "1000000",
          "dao_config_rewards_minimum_increment": "1000000",
          "dao_config_executor_fee": "1000000",
          "dao_config_max_safeboxes": "2000",
          "dao_config_vault_close_time": "1684430000",
          "dao_config_safebox_open_time": "1684431000",
          "dao_config_rewards_remainder_beneficiary": "0x76a914586f33d3cd763ec12ea1829ad834c6e3d345131b88ac"
        }
      },
      "description": "DAO configuration data, required to build Vault and Safebox locking bytecodes.",
      "name": ""
    },
    "dao_genesis_blueprint_data": {
      "data": {
        "bytecode": {
          "genesis_executor_fee": "500000",
          "genesis_bcmr_output_bytecode": "0x76a914e11bc0b45edb8c7caca8b63f4d02eca619dbc23088ac",
          "genesis_message": "'Happy Bitcoin Cash May 2023 upgrade day!'",
          "genesis_designer_pubkey": "designer_key.public_key"
        }
      },
      "description": "DAO genesis blueprint data, required to build Blueprint locking bytecode.",
      "extends": "dao_config_data",
      "name": ""
    },
```

DAO "Vault" transactions:

- Genesis transaction: https://chipnet.imaginary.cash/tx/0df6cfd62fdc314fd40c07425296372069e1b96606439e3096925615449dea68
- Mint No. 1 - locking up exactly the nominal amount + bonus: https://chipnet.imaginary.cash/tx/093bf4536a89fb8d340d4cc0099e9bd1431d42d6d9722cfd635a8a498f40c763
- Mint No. 2 - same: https://chipnet.imaginary.cash/tx/6afeb2a173e8e8b612653307c2a263415aa31df08f29866a0fb350a987f956cf
- Mint No. 3 - user adds his own extra to the safebox: https://chipnet.imaginary.cash/tx/dc6ca48ee3e3a43b0d84ae51b2eeb8b25abcaa7ca37b943fde1c62ff7e1344fa
- Mint No. 4 - user gives up the bonus (vault amount not changed) and locks up exactly the nominal amount: https://chipnet.imaginary.cash/tx/dc6ca48ee3e3a43b0d84ae51b2eeb8b25abcaa7ca37b943fde1c62ff7e1344fa
- Mint No. 5 - user locks exactly the nominal amount, while adding a little to DAO's reward pool: https://chipnet.imaginary.cash/tx/dc6ca48ee3e3a43b0d84ae51b2eeb8b25abcaa7ca37b943fde1c62ff7e1344fa

User P2PKH transactions:

- User transfers No. 2 (p2pkh): https://chipnet.imaginary.cash/tx/716f5ccd31fdd67390dd78b3c04d4d3d180a67c36df772ab16ed263b2847753d

DAO "Safebox" transactions:

- Withdraw No. 2 - user burns his No. 2 keycard NFT with the matching "safebox" NFT to withdraw from the DAO: https://chipnet.imaginary.cash/tx/f8c34d51bbaf8109de3d543986a71032fa4f88b6329aca7f731d34c79bd77410

## License

[MIT No Attribution](https://opensource.org/license/mit-0/).

## Disclaimer

THE DAO CONTRACT, THE DAO INSTANCE(S), AND RELATED SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
