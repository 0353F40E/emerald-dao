# Emerald DAO

Emerald DAO is a simple Bitcoin Cash DAO template which acts as a fixed-term deposit savings vault with optional crowdfunded rewards scheme.
Each depositor interacting with the DAO will be creating a time-locked safebox UTXO and a matching keycard NFT UTXO.
Upon expiry of the time-lock, user will have the option to burn the keycard NFT UTXO to open the safebox and access the deposited BCH + holding reward, or he may keep the keycard NFT indefinitely as a collectible.

## License

[MIT No Attribution](https://opensource.org/license/mit-0/).

## Disclaimer

THE DAO CONTRACT, THE DAO INSTANCE(S), AND RELATED SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
